class Fraccion:
    def __init__(self, num, den):
        self.numerador = num
        self.denominador = den
    
    def __str__(self):
        return '{numerador}/{denominador}'.format(numerador = self.numerador, denominador = self.denominador)

    def suma_fraccion(self, s):
        if self.denominador == s.denominador:
            denominador_comun = s.denominador
        else:
            denominador_comun = self.denominador * s.denominador

        numerador1 = (denominador_comun / self.denominador) * self.numerador
        numerador2 = (denominador_comun / s.denominador) * s.numerador

        self.numerador = numerador1 + numerador2
        self.denominador = denominador_comun

        return Fraccion(self.numerador, self.denominador)

    def resta_fraccion(self, r):
        if self.denominador == r.denominador:
            denominador_comun = r.denominador
        else:
            denominador_comun = self.denominador * r.denominador

        numerador1 = (denominador_comun / self.denominador) * self.numerador
        numerador2 = (denominador_comun / r.denominador) * r.numerador

        numerador = numerador1 - numerador2
        denominador = denominador_comun

        return Fraccion(numerador, denominador)

    def multiplicacion_fraccion(self, m):
        numerador = self.numerador * m.numerador
        denominador = self.denominador * m.denominador
        return Fraccion(numerador, denominador)

    def dividir_fraccion(self, d):
        numerador = self.numerador * d.denominador
        denominador = self.denominador * d.numerador
        if denominador == 0:
            raise Exception ("No se puede dividir por 0")
        return Fraccion(numerador, denominador)
    

