import unittest
from claseFraccion import Fraccion
class TestFraccion(unittest.TestCase):
    def test_suma(self):
        #suma = Fraccion(3/2, 3/4)
        #self.assertEqual(suma, 9/4)
        n1 = Fraccion(1, 3)
        n2 = Fraccion(1, 2)

        suma = n1.suma_fraccion(n2)

        self.assertEqual(suma.numerador, 5)
        self.assertEqual(suma.denominador, 6)

    def test_resta(self):
        n1 = Fraccion(1, 3)
        n2 = Fraccion(1, 2)

        resta = n1.resta_fraccion(n2)

        self.assertEqual(resta.numerador, -1)
        self.assertEqual(resta.denominador, 6)
    
    def test_multiplicacion(self):
        n1 = Fraccion(1, 3)
        n2 = Fraccion(1, 2)

        multiplicacion = n1.multiplicacion_fraccion(n2)

        self.assertEqual(multiplicacion.numerador, 1)
        self.assertEqual(multiplicacion.denominador, 6)

    def test_division(self):
        n1 = Fraccion(1, 3)
        n2 = Fraccion(1, 2)

        division = n1.dividir_fraccion(n2)

        self.assertEqual(division.numerador, 2)
        self.assertEqual(division.denominador, 3)
    
    #def test_division_0(self):
        #n1 = Fraccion(1,2)
        #n2 = Fraccion(0,2)

        #division1 = n1.dividir_fraccion(n2)

        #self.assertEqual(division1.numerador, 2)
        #self.assertEqual(division1.denominador, 0)
        #self.assertRaises(ZeroDivisionError, division1)

    def test_division_0(self):
        Fraccion(1, 0)
        self.assertRaises(ZeroDivisionError)

    def test_str(self):
        n1 = Fraccion(1, 4)

        self.assertEqual('1/4', n1.__str__())


if __name__ == "__main__":
    unittest.main()

